from django.db import models


# Create your models here.
class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100, unique=True)

    class Meta:
        ordering = ["employee_id"]


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
    import_href = models.CharField(max_length=200, unique=True)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    STATUSES = [
        ("Incomplete", "Incomplete"),
        ("Complete", "Complete"),
        ("Canceled", "Canceled"),
    ]
    status = models.CharField(max_length=10, choices=STATUSES, default="Incomplete")
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician, related_name="appointments", on_delete=models.CASCADE
    )
    vip = models.BooleanField(default=False)

    class Meta:
        ordering = ["date_time"]
