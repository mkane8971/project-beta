from common.json import ModelEncoder

from .models import (
    AutomobileVO,
    Salesperson,
    Customer,
    Sale,
)

class SalespersonEncoder(ModelEncoder):
    model= Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "sold",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "salesperson",
        "customer",
        "automobile",
    ]

    encoders = {
        "salesperson" : SalespersonEncoder(),
        "customer" : CustomerEncoder(),
        "automobile" : AutomobileVOEncoder(),
    }
