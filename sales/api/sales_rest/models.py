from django.db import models


class AutomobileVO(models.Model):
    vin= models.CharField(max_length=17, unique=True)
    sold= models.BooleanField(default=False)

    def __str__(self):
        return str(self.vin)

class Salesperson(models.Model):
    first_name= models.CharField(max_length=100)
    last_name= models.CharField(max_length=100)
    employee_id= models.PositiveBigIntegerField(unique=True)

    def __str__(self):
        return str(self.employee_id)


class Customer(models.Model):
    first_name= models.CharField(max_length=100)
    last_name= models.CharField(max_length=100)
    address= models.CharField(max_length=200)
    phone_number= models.CharField(max_length=12, unique=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

class Sale(models.Model):
    price = models.PositiveIntegerField()
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="salesperson",
        on_delete= models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name ="customer",
        on_delete = models.CASCADE,
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete= models.CASCADE,
    )

    def __str__(self):
        return str(self.id)
