from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import requests
import json
from .encoders import (
    SalespersonEncoder,
    CustomerEncoder,
    SaleEncoder,
)
from .models import(
    AutomobileVO,
    Salesperson,
    Customer,
    Sale,
)


def update_automobile(vin, update_sold):
    url= f"http://project-beta-inventory-api-1:8000/api/automobiles/{vin}/"
    requests.put(url, json={"sold": update_sold})


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder= SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson= Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_show_salesperson(request, id):

    if request.method == "GET":

        try:
            salesperson = Salesperson.objects.get(id=id)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    else:
        try:
            salesperson = Salesperson.objects.get(id=id)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_customers(request):

    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder = CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_show_customer(request,id):

    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    else:
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_sales(request):

    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            { "sales" : sales},
            encoder= SaleEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(employee_id=salesperson_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Saleperson Employee ID"},
                status=400,
            )

        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id= customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Customer ID"},
                status=400
            )

        try:
            automobile_id = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_id)
            content["automobile"] = automobile
            if automobile.sold == True:
                return JsonResponse(
                    {"message": "Car has been sold before"}
                )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Vin Number"},
                status=400
            )

        sale = Sale.objects.create(**content)
        automobile.sold = True
        automobile.save()
        update_automobile(automobile.vin, automobile.sold)
        return JsonResponse(
            sale,
            encoder= SaleEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_show_sale(request,id):

    if request.method =="GET":
        sale= Sale.objects.get(id=id)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False
        )
    else:

        try:
            sale = Sale.objects.get(id=id)
            automobile = sale.automobile
            automobile.sold = False
            automobile.save()
            update_automobile(automobile.vin, automobile.sold)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
