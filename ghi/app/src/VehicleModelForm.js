import React, {useState, useEffect} from 'react';
import { NavLink } from 'react-router-dom';


function VehicleModelForm() {
  const [manufacturers, setManufacturers] = useState([])
  const [message, setMessage] = useState(undefined)
  const [formData, setFormData] = useState({
    name: "",
    picture_url: "",
    manufacturer_id: "",
  })

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const modelUrl = 'http://localhost:8100/api/models/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try {
      const response = await fetch(modelUrl, fetchConfig);
      if (response.ok) {
        setFormData({
          name: "",
          picture_url: "",
          manufacturer_id: "",
        });
        setMessage({ type: 'success', success : "Created new Vehicle Model"})
      } else {
          setMessage({ type: 'error', error : "Failed to create"})
    }
    } catch (error) {
      setMessage({ type: 'error', error : "Failed to create"})
    }
  }


  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      //Previous form data is spread (i.e. copied) into our new state object
      ...formData,

      //On top of the that data, we add the currently engaged input key and value
      [inputName]: value
    });
    setMessage(undefined)
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new Vehicle Model</h1>
            <NavLink to="/models" className="btn btn-sm btn-success mb-3">Model List</NavLink>
            <form onSubmit={handleSubmit} id="create-model-form">
                <div className="mb-3">

                    <select
                        onChange={handleFormChange}
                        value={formData.manufacturer_id}
                        required name="manufacturer_id"
                        id="manufacturer_id"
                        className="form-select">

                        <option value="">Choose a manufacturer</option>
                        {manufacturers.map(manufacturer => {
                            return (
                            <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                            )
                        })}
                    </select>

                </div>
                <div className="form-floating mb-3">

                    <input
                        onChange={handleFormChange}
                        value={formData.name}
                        placeholder="name"
                        required type="text"
                        name="name"
                        id="name"
                        className="form-control" />
                    <label htmlFor="name">Model</label>

                </div>


                <div className="form-floating mb-3">

                    <input
                        onChange={handleFormChange}
                        value={formData.picture_url}
                        placeholder="picture_url"
                        required type="url"
                        name="picture_url"
                        id="picture_url"
                        className="form-control" />
                    <label htmlFor="picture_url">Picture</label>

                </div>

                <button className="btn btn-success">Create</button>
            </form>
            <div>
              {message && (
                <div className="shadow p-4 mt-4">
                  {message.type === 'success' ? message.success : message.error}
                  <div>
                    <NavLink to="/automobiles/new" className="btn btn-sm btn-success">Add New Automobile to Inventory</NavLink>
                  </div>
                </div>
              )}
            </div>
        </div>
      </div>
    </div>
  );
}

export default VehicleModelForm;
