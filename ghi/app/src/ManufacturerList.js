import { NavLink } from 'react-router-dom';
import { useState, useEffect } from 'react';

const ManufacturerList = () => {
    const [manufacturers, setManufacturers] = useState([]);

    async function getManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const { manufacturers } = await response.json();
            setManufacturers(manufacturers);
        } else {
            console.error(response);
        }
    }

    useEffect(() => {
        getManufacturers();
    }, [])


    const handleDelete = async (event) => {
        event.preventDefault();
        const manufacturer_id = event.target.value;
        const url = `http://localhost:8100/api/manufacturers/${manufacturer_id}`;
        const fetchConfig = {
            method: "delete",
            headers: { 'Content-Type': 'application/json' }
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            getManufacturers();
        }
    };
    return (
        <>
            <div className="row text-center mt-3">
                <h1 className="mb-2">Manufacturers</h1>
                <p>
                    <NavLink to="/manufacturers/new" className="btn btn-sm btn-primary">Add New Manufacturer</NavLink>
                </p>
            </div>
            <div className="row justify-content-center text-center">
                <div className='col-4'>
                    <table className='table table-striped table-hover'>
                        <thead>
                            <tr>
                                <th colSpan={"2"}>Manufacturer</th>
                            </tr>
                        </thead>
                        <tbody>
                            {manufacturers.length !== 0 ? manufacturers.map(manufacturer => {
                                return (
                                    <tr key={manufacturer.id}>
                                        <td>{manufacturer.name}</td>
                                        <td><button className="btn-sm btn-danger" onClick={handleDelete} value={manufacturer.id}>Delete</button></td>
                                    </tr>
                                );
                            }) :
                                <tr><td>No Manufacturers in Inventory</td></tr>}
                        </tbody>
                    </table>

                </div>
            </div>
        </>
    );
}

export default ManufacturerList;
