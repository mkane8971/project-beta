import { NavLink } from 'react-router-dom';
import { useState, useEffect } from 'react';

const TechList = () => {
    const [techs, setTechs] = useState([]);
    const [deleteTech, setDeleteTech] = useState([]);

    async function getTechs() {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (response.ok) {
            const { technicians } = await response.json();
            setTechs(technicians);
        } else {
            console.error(response);
        }
    }

    const handleDelete = async (tech_id) => {
        const url = `http://localhost:8080/api/technicians/${tech_id}`;
        const fetchConfig = {
            method: "delete",
            headers: { 'Content-Type': 'application/json' }
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            getTechs();
        }
    };

    const deleteConfirm = async (id) => {
        setDeleteTech(id);
    };

    useEffect(() => {
        getTechs();
    }, [])

    return (
        <>
            <div className="row text-center mt-3">
                <h1 className="mb-2">Technicians</h1>
                <p><NavLink to="/technicians/new" className="btn btn-sm btn-primary">Add New Technician</NavLink></p>
            </div>
            <div className="row justify-content-center text-center">
                <div className='col-10'>
                    <table className='table table-striped table-hover'>
                        <thead>
                            <tr>
                                <th>Employee ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Appointments</th>
                                <th>Delete Tech</th>
                            </tr>
                        </thead>
                        <tbody>
                            {techs.length !== 0 ? techs.map(tech => {
                                return (
                                    <tr key={tech.id} value={tech.id}>
                                        <td>{tech.employee_id}</td>
                                        <td>{tech.first_name}</td>
                                        <td>{tech.last_name}</td>
                                        <td>{tech.appointments}</td>
                                        <td>
                                            {deleteTech === tech.id ?
                                                <div className="container text-center">
                                                    <h1 className="text-danger">!!WARNING!!</h1>
                                                    <h2>**Deleting this tech will delete <strong>{tech.appointments} appointments</strong> assigned to this tech**</h2>
                                                    <div>
                                                        <button
                                                            type="button"
                                                            className="btn btn-danger mb-3"
                                                            onClick={() => handleDelete(tech.id)}>Confirm Delete Tech
                                                        </button>
                                                    </div>
                                                    <div>
                                                        <button
                                                            type="button"
                                                            className="btn btn-success"
                                                            onClick={() => setDeleteTech(null)}>CANCEL
                                                        </button>
                                                    </div>
                                                </div>
                                            :
                                            <button className="btn-sm btn-danger" onClick={() => deleteConfirm(tech.id)} value={tech.id}>Delete</button>
                                            }
                                        </td>
                                    </tr>
                                );
                            }) :
                                <tr>
                                    <td colSpan={'5'}>No Technicians :(</td>
                                </tr>
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    );
}

export default TechList;
