import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function AppointmentForm() {
    const navigate = useNavigate();
    const [formData, setFormData] = useState({
        vin: '',
        customer: '',
        date: '',
        time: '',
        technician: '',
        reason: ''
    });

    const [techs, setTechs] = useState([]);

    async function getTechs() {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (response.ok) {
            const { technicians } = await response.json();
            setTechs(technicians);
        } else {
            console.error(response);
        }
    }

    useEffect(() => {
        getTechs();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8080/api/appointments/';

        const date_time = new Date(formData.date + " " + formData.time).toJSON();
        const data = {
            vin: formData.vin,
            customer: formData.customer,
            date_time: date_time,
            technician: formData.technician,
            reason: formData.reason
        };
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };


        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                vin: '',
                customer: '',
                date: '',
                time: '',
                technician: '',
                reason: ''
            });
            navigate("/appointments")
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create New Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">

                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Automobile VIN..." required type="text" name="vin" id="vin" maxLength='17' className="form-control" value={formData.vin} />
                            <label htmlFor="vin">Automobile VIN...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Customer..." required type="text" name="customer" id="customer" className="form-control" value={formData.customer} />
                            <label htmlFor="customer">Customer...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} required type="date" name="date" id="date" className="form-control" value={formData.date} />
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} required type="time" name="time" id="time" className="form-control" value={formData.time} />
                            <label htmlFor="time">Time</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} placeholder="Choose a technician..." required name="technician" id="technician" className="form-select" value={formData.technician}>
                                <option value="">Choose a technician...</option>
                                {techs.map(technician => {
                                    return (
                                        <option key={technician.id} value={technician.employee_id}>{technician.first_name} {technician.last_name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Reason..." required type="text" name="reason" id="reason" className="form-control" value={formData.reason} />
                            <label htmlFor="reason">Reason...</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default AppointmentForm;
