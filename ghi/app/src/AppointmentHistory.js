import { NavLink } from 'react-router-dom';
import { useState, useEffect } from 'react';

const AppointmentHistory = () => {
    const [complete, setComplete] = useState([]);

    async function getAppointments() {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const { appointments } = await response.json();
            const complete = [];

            for (const appt of appointments){
                if (appt.status !== 'Incomplete') {
                    complete.push(appt);
                }
            }
            setComplete(complete);
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        getAppointments();
    }, [])

    const [search, setSearch] = useState('');

    const handleFormChange = (e) => {
        const value = e.target.value;
        setSearch(value);
    }
    const handleClear = (e) => {
        setSearch('');
    }

    return (
        <>
            <div className="row text-center mt-3">
                <h1 className="mb-2">Service Appointments</h1>
                <p><NavLink to="/appointments/new" className="btn btn-sm btn-primary">Schedule New Appointment</NavLink></p>
                <p><NavLink to="/appointments" className="btn btn-sm btn-secondary">Service Appointments</NavLink></p>
            </div>
            <div className="row align-items-center">
                <div className="col-2">
                    <div className="input-group">
                        <div className="form-floating">
                            <input placeholder="Search VIN" onChange={handleFormChange} type="text" name="search" id="search" className="form-control" value={search} />
                            <label htmlFor="search">Search VIN</label>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <button className="btn btn-danger" onClick={handleClear}>Clear</button>
                </div>
            </div>
            <div className="row justify-content-center text-center">
                <div className='col'>
                    <table className='table table-striped table-hover'>
                        <thead>
                            <tr>
                                <th>VIN</th>
                                <th>VIP?</th>
                                <th>Customer</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Technician</th>
                                <th>Reason</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {complete.map(appointment => {
                                if (appointment.vin === search || search === '') {
                                    return (
                                        <tr key={appointment.id}>
                                            <td>{appointment.vin}</td>
                                            <td>{appointment.vip ? 'Yes' : 'No'}</td>
                                            <td>{appointment.customer}</td>
                                            <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                                            <td>{new Date(appointment.date_time).toLocaleTimeString(undefined, { hour: 'numeric', minute: 'numeric' })}</td>
                                            <td>{appointment.technician}</td>
                                            <td>{appointment.reason}</td>
                                            <td>{appointment.status}</td>
                                        </tr>
                                    );
                                } else {
                                    return null;
                                }
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    );
}

export default AppointmentHistory;
