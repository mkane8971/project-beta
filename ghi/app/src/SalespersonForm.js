import React, {useState} from 'react';
import { NavLink } from 'react-router-dom';
function SalespersonForm() {

    const [formData, setFormData] = useState({
        first_name: "",
        last_name: "",
        employee_id: "",
    })
    const [message, setMessage] = useState(undefined)


    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json',
        },
        };
        try {
            const response = await fetch(url, fetchConfig);
            if (response.ok) {
                setFormData({
                    first_name: "",
                    last_name: "",
                    employee_id: "",
                });
                setMessage({ type: 'success', success : "Created new salesperson"})
            } else {
                setMessage({ type: 'error', error : "Failed to create"})
            }
        } catch (error) {
            setMessage({ type: 'error', error : "Failed to create"})
        }
     }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
        //Previous form data is spread (i.e. copied) into our new state object
        ...formData,

        //On top of the that data, we add the currently engaged input key and value
        [inputName]: value
        });
        setMessage(undefined)
    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a Salesperson to the Roster</h1>
                <NavLink to="/salespeople" className="btn btn-sm btn-success mb-3">Show Salesperson Roster</NavLink>
                <form onSubmit={handleSubmit} id="create-model-form">
                    <div className="mb-3">

                        <label htmlFor="first_name">First Name</label>
                        <input
                            onChange={handleFormChange}
                            value={formData.first_name}
                            placeholder="First Name"
                            required name="first_name"
                            id="first_name"
                            className="form-control" />



                    </div>
                    <div className="mb-3">

                        <label htmlFor="last_name">Last Name</label>
                        <input
                            onChange={handleFormChange}
                            value={formData.last_name}
                            placeholder="Last Name"
                            required type="text"
                            name="last_name"
                            id="last_name"
                            className="form-control" />


                    </div>


                    <div className="mb-3">

                        <label htmlFor="employee_id">Employee ID</label>
                        <input
                            onChange={handleFormChange}
                            value={formData.employee_id}
                            placeholder="Employee ID"
                            required type="number"
                            min="1"
                            max="199"
                            name="employee_id"
                            id="employee_id"
                            className="form-control" />

                    </div>

                    <button className="btn btn-success">Create</button>
                </form>
                <div>
                    {message && (
                        <div className="shadow p-4 mt-4">
                            {message.type === 'success' ? message.success : message.error}
                            <div>
                                <NavLink to="/sales/new" className="btn btn-sm btn-success">Add New Sale Record</NavLink>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </div>
        </div>
    );
}

export default SalespersonForm;
