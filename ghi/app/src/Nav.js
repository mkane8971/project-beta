import { NavLink } from 'react-router-dom';

function Nav() {
	return (
		<nav className="navbar navbar-expand-lg navbar-dark bg-success">
			<div className="container-fluid">
				<NavLink className="navbar-brand" to="/">CarCar</NavLink>
				<button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span className="navbar-toggler-icon"></span>
				</button>
				<div className="collapse navbar-collapse" id="navbarSupportedContent">
					<ul className="navbar-nav me-auto mb-2 mb-lg-0">
						<li className="nav-link dropdown">
							<NavLink className="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" to="#">Inventory</NavLink>
							<ul className='dropdown-menu'>
								<li>
									<NavLink className="dropdown-item" to="manufacturers">Manufacturers</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/models">Models</NavLink>
								</li>
								<li className="nav-item">
									<NavLink className="dropdown-item" to="/automobiles">Automobiles Available for Sale</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-link dropdown">
							<NavLink className="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" to="#">Services</NavLink>
							<ul className='dropdown-menu'>
								<li className="nav-item">
									<NavLink className="dropdown-item" aria-current="page" to="technicians">Technicians</NavLink>
								</li>
								<li className="nav-item">
									<NavLink className="dropdown-item" aria-current="page" to="appointments">Service Appointments</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-link dropdown">
							<NavLink className="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" to="#">Sales</NavLink>
							<ul className='dropdown-menu'>
								<li className="nav-item">
									<NavLink className="dropdown-item" to="/customers">Customers</NavLink>
								</li>
								<li className="nav-item">
									<NavLink className="dropdown-item" to="/salespeople">Salespeople</NavLink>
								</li>
								<li className="nav-item">
								<NavLink className="dropdown-item" to="/sales">List All Sales</NavLink>
								</li>
								<li className="nav-item">
								<NavLink className="dropdown-item" to="/sales/history">Salesperson Sales History</NavLink>
								</li>

							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	)
}

export default Nav;
